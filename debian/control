Source: nvidia-nccl
Section: contrib/libs
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13),
               nvidia-cuda-toolkit-gcc,
               python3-all,
Homepage: https://github.com/NVIDIA/nccl
Vcs-Browser: https://salsa.debian.org/nvidia-team/nvidia-nccl
Vcs-Git: https://salsa.debian.org/nvidia-team/nvidia-nccl.git
Rules-Requires-Root: no

Package: libnccl2
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: libnccl.so.2
Description: NVIDIA Optimized primitives for inter-GPU communication
 NCCL (pronounced "Nickel") is a stand-alone library of standard communication
 routines for GPUs, implementing all-reduce, all-gather, reduce, broadcast,
 reduce-scatter, as well as any send/receive based communication pattern. It
 has been optimized to achieve high bandwidth on platforms using PCIe, NVLink,
 NVswitch, as well as networking using InfiniBand Verbs or TCP/IP sockets. NCCL
 supports an arbitrary number of GPUs installed in a single node or across
 multiple nodes, and can be used in either single- or multi-process (e.g., MPI)
 applications.
 .
 This package contains the shared objects.

Package: libnccl-dev
Section: contrib/libdevel
Architecture: amd64 arm64 ppc64el
Depends: libnccl2 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Provides: libnccl.so
Description: NVIDIA Optimized primitives for inter-GPU communication (development)
 NCCL (pronounced "Nickel") is a stand-alone library of standard communication
 routines for GPUs, implementing all-reduce, all-gather, reduce, broadcast,
 reduce-scatter, as well as any send/receive based communication pattern. It
 has been optimized to achieve high bandwidth on platforms using PCIe, NVLink,
 NVswitch, as well as networking using InfiniBand Verbs or TCP/IP sockets. NCCL
 supports an arbitrary number of GPUs installed in a single node or across
 multiple nodes, and can be used in either single- or multi-process (e.g., MPI)
 applications.
 .
 This package contains the development files.
